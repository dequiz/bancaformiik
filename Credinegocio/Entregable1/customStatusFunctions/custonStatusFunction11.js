(function(_value){
	
	helper.Log("CustomStatus "+_value.containerId+" inicio: " + getContainerCustomStatus(_value.containerId));  
	const enCaptura = 0;
	const enValidacion = 1;
	const completa = 2;

	const statusValidacionHijo2 = 3;
	const statusValidacionHijo3 = 1;
	const statusCompletoHijo4 = 1;

	const requisitoInfoPersonal = "100-00000-0000-0112"
	const requisitoNegocio = "100-00000-0000-0113"
	const requisitoRecomendacion = "100-00000-0000-0114"

	var returnValue = enCaptura;
	var status = getContainerStatus(_value.containerId);
	var statusHijo2 = getContainerCustomStatus(requisitoInfoPersonal);
	var statusHijo3 = getContainerCustomStatus(requisitoNegocio);
	var statusHijo4 = getContainerCustomStatus(requisitoRecomendacion);
	
	switch(status) {
		case CONSTANTS.CONTAINER_STATUS.CLOSED:
			helper.Log("CustomStatus "+_value.containerId+" case 1")
			returnValue = enCaptura; 
			break;
		case CONSTANTS.CONTAINER_STATUS.VALID:
			if (statusHijo2 == statusValidacionHijo2 || statusHijo3 == statusValidacionHijo3) {
				helper.Log("CustomStatus "+_value.containerId+" case 2")
				returnValue = enValidacion; 
			} else {
				if (statusHijo4 == statusCompletoHijo4) {
					helper.Log("CustomStatus "+_value.containerId+" case 3")
					returnValue = completa; 
				} else {
					helper.Log("CustomStatus "+_value.containerId+" case 4")
					returnValue = enCaptura; 
				}
			}			
			break;
	}

	helper.Log("CustomStatus "+_value.containerId+" fin: " + returnValue);  

	return returnValue;
})(_v_);