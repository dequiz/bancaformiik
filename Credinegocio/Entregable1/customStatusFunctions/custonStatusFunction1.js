(function(_value){
	
	helper.Log("CustomStatus "+_value.containerId+" inicio: " + getContainerCustomStatus(_value.containerId));  
	const enLlenadoSolicitud = 0;
	const enValidacion = 1;
	const enAnalisis = 2;
	const creditoAprobado = 3;
	const creditoRechazado = 4;

	const statusHijo1EnCaptura = 0;
	const statusHijo1EnValidacion = 1;
	const statusHijo1Completa = 2;

	const statusHijo2PorTomarDecision = 0;
	const statusHijo2Aceptado = 1;
	const statusHijo2Rechazado = 2;
	const statusHijo2Devuelto = 3;

	const requisitoSolicitud = "100-00000-0000-0011"
	const requisitoAnalisis = "100-00000-0000-0012"

	var returnValue = enLlenadoSolicitud;
	var status = getContainerStatus(_value.containerId);
	var statusHijo1 = getContainerCustomStatus(requisitoSolicitud);
	var statusHijo2 = getContainerCustomStatus(requisitoAnalisis);

	switch(statusHijo1) {
		case statusHijo1EnCaptura:
			returnValue = enLlenadoSolicitud; 
			break;
		case statusHijo1EnValidacion:
			returnValue = enValidacion; 
			break;
		case statusHijo1Completa:

			switch(statusHijo2) {
				case statusHijo2PorTomarDecision:
					returnValue = enAnalisis; 
					break;
				case statusHijo2Aceptado:
					returnValue = creditoAprobado; 
					break;
				case statusHijo2Rechazado:
					returnValue = creditoRechazado; 
					break;
				case statusHijo2Devuelto:
					returnValue = enLlenadoSolicitud; 
					break;
			}

			break;
	}

	helper.Log("CustomStatus "+_value.containerId+" fin: " + returnValue);

	return returnValue;
})(_v_);