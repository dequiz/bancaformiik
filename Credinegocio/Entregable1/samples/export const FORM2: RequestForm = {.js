export const FORM2: RequestForm = {
  description: 'Información del cliente y su familia',
  isReadOnly: false,
  isToValidate: false,

  sections: [
    {
      description: 'Datos personales',
      rows: [
        {
          subRequestForm: null,
          elementSize: FormElementSize.COL_2,
          elements: [
            {
              label: 'Tipo de documento',
              fullName: 'Titular.TipoDocumento',
              elementType: FormElement.SELECT,
              data: [
                {
                  value: '0',
                  viewValue: 'Opcion 1'
                },
                {
                  value: '2',
                  viewValue: 'Opcion 2'
                }
              ],
              isRequired: false
            }
            {
              label: 'Tipo de documento',
              fullName: 'Titular.NumeroDocumento',
              elementType: FormElement.SELECT,
              data: [
                {
                  value: '0',
                  viewValue: 'Opcion 1'
                },
                {
                  value: '2',
                  viewValue: 'Opcion 2'
                }
              ],
              isRequired: false
            }
          ]
        }
      ]
    }
  ]
};