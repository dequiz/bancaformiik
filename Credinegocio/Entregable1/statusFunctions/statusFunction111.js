(function(_value){
	
	helper.Log("Cambio estado "+_value.containerId+" inicio: "+getContainerStatus(_value.containerId));
	var returnValue = getContainerStatus(_value.containerId);
	
	switch(returnValue) {
		case CONSTANTS.CONTAINER_STATUS.CLOSED:
			returnValue = CONSTANTS.CONTAINER_STATUS.OPEN; 
			break;
		case CONSTANTS.CONTAINER_STATUS.OPEN:
			if (getValueByFullName(_value.containerId, "NumeroCliente") != null) {
				returnValue = CONSTANTS.CONTAINER_STATUS.VALID; 
			} else {
				if (getValueByFullName(_value.containerId, "FolioCredito") != null) {
					returnValue = CONSTANTS.CONTAINER_STATUS.VALID; 
				} else {
					if (!isNull(_value.containerId,["TipoDocumento","NumeroDocumento"])) {
						returnValue = CONSTANTS.CONTAINER_STATUS.VALID; 
					}
				}
			}
			break;
		default:
			returnValue = getContainerStatus(_value.containerId)
	} 

	helper.Log("Cambio estado "+_value.containerId+" fin: "+getContainerStatus(_value.containerId));
	
	return returnValue;
})(_v_);