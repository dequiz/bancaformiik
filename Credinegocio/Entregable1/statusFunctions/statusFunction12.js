(function(_value){

	const requisitoTomaDecision = "100-00000-0000-0121";

	helper.Log("Cambio estado " + _value.containerId + " inicio: " + getContainerStatus(_value.containerId));
	var returnValue = getContainerStatus(_value.containerId);
	
	switch(returnValue) {
		case CONSTANTS.CONTAINER_STATUS.CLOSED:
			if (getContainerStatus(requisitoTomaDecision) == CONSTANTS.CONTAINER_STATUS.OPEN) {
				returnValue = CONSTANTS.CONTAINER_STATUS.OPEN; 
			}
			break;
		case CONSTANTS.CONTAINER_STATUS.OPEN:
			if (getContainerStatus(requisitoTomaDecision) == CONSTANTS.CONTAINER_STATUS.CLOSED) {
				returnValue = CONSTANTS.CONTAINER_STATUS.CLOSED; 
			}
			break;
		default:
			returnValue = getContainerStatus(_value.containerId)
	}

	helper.Log("Cambio estado " + _value.containerId + " fin: " + returnValue);
	
	return returnValue;
})(_v_);