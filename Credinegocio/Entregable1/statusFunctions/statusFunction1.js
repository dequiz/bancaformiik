(function(_value){

	const requisitoSolicitud = "100-00000-0000-0011";
	const requisitoAnalisis = "100-00000-0000-0012";

	helper.Log("Cambio estado "+_value.containerId+" inicio: "+getContainerStatus(_value.containerId));
	var returnValue = getContainerStatus(_value.containerId);
	
	switch(returnValue) {
		case CONSTANTS.CONTAINER_STATUS.CLOSED:
			if (getContainerStatus(requisitoSolicitud) == CONSTANTS.CONTAINER_STATUS.OPEN || getContainerStatus(requisitoAnalisis) == CONSTANTS.CONTAINER_STATUS.OPEN) {
				returnValue = CONSTANTS.CONTAINER_STATUS.OPEN; 
			}
			break;
		case CONSTANTS.CONTAINER_STATUS.OPEN:
			if (getContainerStatus(requisitoSolicitud) == CONSTANTS.CONTAINER_STATUS.VALID && getContainerStatus(requisitoAnalisis) == CONSTANTS.CONTAINER_STATUS.CLOSED) {
				returnValue = CONSTANTS.CONTAINER_STATUS.CLOSED; 
			}
			break;
		default:
			returnValue = getContainerStatus(_value.containerId)
	}

	helper.Log("Cambio estado "+_value.containerId+" fin: "+getContainerStatus(_value.containerId));
	
	return returnValue;
})(_v_);