(function(_value){

	const requisitoBusqueda = "100-00000-0000-0111";
	const requisitoToma = "100-00000-0000-0121"
	const acceptedValue = "0";
	const rejectedValue = "1";
	const estadoTomaDecisionDevuelto = "3";
	
	helper.Log("Cambio estado " + _value.containerId + " inicio: " + getContainerStatus(_value.containerId));
	var returnValue = getContainerStatus(_value.containerId);
	var estadoTomaDecision = getContainerCustomStatus(requisitoToma);
	
	switch(returnValue) {
		case CONSTANTS.CONTAINER_STATUS.CLOSED:
			helper.Log("112 entré a case Closed");
			if (getContainerStatus(requisitoBusqueda) == CONSTANTS.CONTAINER_STATUS.VALID) {
				returnValue = CONSTANTS.CONTAINER_STATUS.OPEN; 
			}
			break;
		case CONSTANTS.CONTAINER_STATUS.OPEN:
			helper.Log("112 entré a case Open");
			if (!isNull(_value.containerId,["Titular.TipoDocumento", "Titular.PaisExpedicion", "Titular.NumeroDocumento", "Titular.PrimerNombre", "Titular.PrimerApellido", "Titular.FechaNacimiento", "Titular.Genero", "Titular.EstadoCivil",  "TipoDomicilio", "ContactoTitular.Direccion.CalleYNumero", "ContactoTitular.Direccion.DepartamentoMunicipioBarrio", "Titular.NivelEducativo", "TitularEsEmpleado", "NumeroMiembrosFamilia", "TotalPersonasACargo", "NumeroHijos", "TitularCabezaHogar", "TitularSabeLeerEscribir", "TiempoResidencia"]) 
				&& !isEmpty(_value.containerId,["Titular.TipoDocumento", "Titular.PaisExpedicion", "Titular.NumeroDocumento", "Titular.PrimerNombre", "Titular.PrimerApellido", "Titular.FechaNacimiento", "Titular.Genero", "Titular.EstadoCivil",  "TipoDomicilio", "ContactoTitular.Direccion.CalleYNumero", "ContactoTitular.Direccion.DepartamentoMunicipioBarrio", "Titular.NivelEducativo", "TitularEsEmpleado", "NumeroMiembrosFamilia", "TotalPersonasACargo", "NumeroHijos", "TitularCabezaHogar", "TitularSabeLeerEscribir", "TiempoResidencia"])) {
				returnValue = CONSTANTS.CONTAINER_STATUS.TOCONFIRM; 
	        	helper.Log("ResultadoDecision");
			}
			break;
		case CONSTANTS.CONTAINER_STATUS.TOCONFIRM:
			helper.Log("112 entré a case To Confirm");
			helper.Log("Resultado de isValid: "+ isValid(_value.containerId,["Titular.TipoDocumento", "Titular.PaisExpedicion", "Titular.NumeroDocumento", "Titular.PrimerNombre", "Titular.PrimerApellido", "Titular.FechaNacimiento", "Titular.Genero", "Titular.EstadoCivil", "TipoDomicilio", "ContactoTitular.Direccion.CalleYNumero", "ContactoTitular.Direccion.DepartamentoMunicipioBarrio", "Titular.NivelEducativo", "TitularEsEmpleado", "NumeroMiembrosFamilia", "TotalPersonasACargo", "NumeroHijos", "TitularCabezaHogar", "TitularSabeLeerEscribir", "TiempoResidencia"]));
			if (isValid(_value.containerId,["Titular.TipoDocumento", "Titular.PaisExpedicion", "Titular.NumeroDocumento", "Titular.PrimerNombre", "Titular.PrimerApellido", "Titular.FechaNacimiento", "Titular.Genero", "Titular.EstadoCivil", "TipoDomicilio", "ContactoTitular.Direccion.CalleYNumero", "ContactoTitular.Direccion.DepartamentoMunicipioBarrio", "Titular.NivelEducativo", "TitularEsEmpleado", "NumeroMiembrosFamilia", "TotalPersonasACargo", "NumeroHijos", "TitularCabezaHogar", "TitularSabeLeerEscribir", "TiempoResidencia"])) {
				returnValue = CONSTANTS.CONTAINER_STATUS.CONFIRMED; 
			} else {
				returnValue = CONSTANTS.CONTAINER_STATUS.REJECTED; 
			}
			break;
		case CONSTANTS.CONTAINER_STATUS.CONFIRMED: 
			helper.Log("112 entré a case Confirmed");
			helper.Log("Estado 121 visto desde el 112: " + estadoTomaDecision);
			runCondition([requisitoToma]);
			estadoTomaDecision = getContainerCustomStatus(requisitoToma);
			if(estadoTomaDecision == estadoTomaDecisionDevuelto) {
				returnValue = CONSTANTS.CONTAINER_STATUS.OPEN; 
				saveValueHelper("100-00000-0000-0121", {"ResultadoDecision":null});
				reset(["100-00000-0000-0113","100-00000-0000-0114","100-00000-0000-0011"]);
				saveValueHelper(_value.containerId, {Reopened:"true"});
			}
			break;
		default:
			returnValue = getContainerStatus(_value.containerId);
	}

	helper.Log("Cambio estado " + _value.containerId + " fin: " + returnValue);
	
	return returnValue;
})(_v_);