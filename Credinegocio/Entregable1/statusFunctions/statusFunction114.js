(function(_value){

	const requisitoNegocio = "100-00000-0000-0113";
	const requisitoToma = "100-00000-0000-0121";  
	const devuelto = "3";
	
	helper.Log("Cambio estado "+_value.containerId+" inicio: "+getContainerStatus(_value.containerId));
	var returnValue = getContainerStatus(_value.containerId);
	
	switch(returnValue) {
		case CONSTANTS.CONTAINER_STATUS.CLOSED:
			if (getContainerStatus(requisitoNegocio) == CONSTANTS.CONTAINER_STATUS.TOCONFIRM) {
				returnValue = CONSTANTS.CONTAINER_STATUS.OPEN; 
			}
			break;
		case CONSTANTS.CONTAINER_STATUS.OPEN:
			if (!isNull(_value.containerId,["MontoRecomendado", "NumeroCuotasRecomendado", "ValorCuotasRecomendado", "FormaPagoRecomendado"]) 
				&& !isEmpty(_value.containerId,["MontoRecomendado", "NumeroCuotasRecomendado", "ValorCuotasRecomendado", "FormaPagoRecomendado"])) {
				returnValue = CONSTANTS.CONTAINER_STATUS.VALID; 
			}
			break;
		case CONSTANTS.CONTAINER_STATUS.VALID:
			if (getContainerCustomStatus(requisitoToma) == devuelto) {
				returnValue = CONSTANTS.CONTAINER_STATUS.OPEN; 
			}
			break;
		default:
			returnValue = getContainerStatus(_value.containerId)
	}	 

	helper.Log("Cambio estado "+_value.containerId+" fin: "+getContainerStatus(_value.containerId));
	
	return returnValue;
})(_v_);