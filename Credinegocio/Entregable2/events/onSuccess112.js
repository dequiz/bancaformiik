(function(value){
	saveValueHelper("100-00000-0000-0112", {Titular:{PrimerNombre:value.response.name}});
	saveValueHelper("100-00000-0000-0112", {Titular:{SegundoNombre:value.response.secondName}});
	saveValueHelper("100-00000-0000-0112", {Titular:{PrimerApellido:value.response.middleName}});
	saveValueHelper("100-00000-0000-0112", {Titular:{SegundoApellido:value.response.lastName}});
	saveValueHelper("100-00000-0000-0112", {Titular:{FechaNacimiento:value.response.birthDate}});
	saveValueHelper("100-00000-0000-0112", {Titular:{EstadoCivil:[value.response.civilStatus]}});
	saveValueHelper("100-00000-0000-0112", {Titular:{Genero:[value.response.gender]}});
	saveValueHelper("100-00000-0000-0112", {Titular:{TipoDocumento:[value.response.documentType]}});
	saveValueHelper("100-00000-0000-0112", {Titular:{NumeroDocumento:value.response.documentNumber}});
	saveValueHelper("100-00000-0000-0112", {Titular:{PaisExpedicion:[value.response.expeditionPlace]}});
	saveValueHelper("100-00000-0000-0112", {Titular:{NivelEducativo:[value.response.educationLevel]}});

	saveValueHelper("100-00000-0000-0112", {TipoDomicilio:[value.response.typeOfAddress]});

	saveValueHelper("100-00000-0000-0112", {ContactoTitular:{Direccion:{DepartamentoMunicipioBarrio:[value.response.department,value.response.municipality,value.response.neighborhood]}}});
	saveValueHelper("100-00000-0000-0112", {ContactoTitular:{Direccion:{CalleYNumero:value.response.streetAddress}}});
	saveValueHelper("100-00000-0000-0112", {ContactoTitular:{Telefono:value.response.phoneNumber}});
	saveValueHelper("100-00000-0000-0112", {ContactoTitular:{TelefonoCelular:value.response.cellNumber}});
	saveValueHelper("100-00000-0000-0112", {ContactoTitular:{CorreoElectronico:value.response.mail}});
	
})(_v_);

