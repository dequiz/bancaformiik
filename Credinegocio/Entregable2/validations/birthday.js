(function (_value){
	var expreg = (_value===null) || (_value==="");
	if ((!expreg) && 
		(typeof _value == "string") &&
		(/^(0[1-9]|[12][0-9]|3[01])[\/\-.](0[1-9]|1[012])[\/\-]((19)[1-9](\d)|(((19)[2-9]))|(20)[0][0-2])$/.test(_value))) {
		
		var month = Number(_value.substring(3,5))-1;
		var day = Number(_value.substring(0,2));
		var year = Number(_value.substring(6,10));
		var dateValue = new Date(year, month, day);

		expreg = dateValue.getDate()==day;
	}
	return (expreg);
})(_value);

