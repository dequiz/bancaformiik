(function(_value){
	
	helper.Log("CustomStatus "+_value.containerId+" inicio: " + getContainerCustomStatus(_value.containerId));  
	const sinComenzar = 0;
	const incompleto = 1;
	const listoParaBusqueda = 2;

	var returnValue = 0;
	var status = getContainerStatus(_value.containerId);
	
	switch(status) {
		case CONSTANTS.CONTAINER_STATUS.CLOSED:
			returnValue = sinComenzar; 
			break;
		case CONSTANTS.CONTAINER_STATUS.OPEN:
			if (getValueByFullName(_value.containerId, "NumeroCliente") != null) {
				returnValue = listoParaBusqueda; 
			} else {
				if (getValueByFullName(_value.containerId, "FolioCredito") != null) {
					returnValue = listoParaBusqueda; 
				} else {
					if (!isNull(_value.containerId,["TipoDocumento","NumeroDocumento"])) {
						returnValue = listoParaBusqueda;
					} else {
						returnValue = incompleto;
					}
				}
			}
			break;
		case CONSTANTS.CONTAINER_STATUS.VALID:
			returnValue = listoParaBusqueda;
			break;
	}

	helper.Log("CustomStatus "+_value.containerId+" fin: " + returnValue);

	return returnValue;
})(_v_);