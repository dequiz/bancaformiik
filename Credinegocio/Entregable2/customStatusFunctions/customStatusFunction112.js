(function(_value){
	
	helper.Log("CustomStatus "+_value.containerId+" inicio: " + getContainerCustomStatus(_value.containerId));  
	const porCapturar = 0;
	const consultaExitosa = 1;
	const consultaFallida = 2;
	const porValidar = 3;
	const validado = 4;
	const rechazado = 5;
	const reopened = 6;

	var returnValue = 0;
	var status = getContainerStatus(_value.containerId);
	
	switch(status) {
		case CONSTANTS.CONTAINER_STATUS.CLOSED:
			returnValue = porCapturar; 
			break;
		case CONSTANTS.CONTAINER_STATUS.OPEN:
			if (getValueByFullName(_value.containerId, "Reopened") == "true") {
				returnValue = reopened;
			} else {
				if (getValueByFullName(_value.containerId, "Titular.NumeroDocumento") != null) {
					returnValue = consultaExitosa; 
				}  
			}
			break;
		case CONSTANTS.CONTAINER_STATUS.ERROR:
			returnValue = consultaFallida; 
			break;
		case CONSTANTS.CONTAINER_STATUS.TOCONFIRM:
			returnValue = porValidar;
			break;
		case CONSTANTS.CONTAINER_STATUS.CONFIRMED:
			returnValue = validado;
			break;
		case CONSTANTS.CONTAINER_STATUS.REJECTED:
			returnValue = rechazado;
			break;

	}

	helper.Log("CustomStatus "+_value.containerId+" fin: " + returnValue);

	return returnValue;
})(_v_);