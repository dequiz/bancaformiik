(function(_value){
	
	helper.Log("CustomStatus "+_value.containerId+" inicio: " + getContainerCustomStatus(_value.containerId));  
	const porCapturar = 0;
	const completo = 1;

	var returnValue = 0;
	var status = getContainerStatus(_value.containerId);
	
	switch(status) {
		case CONSTANTS.CONTAINER_STATUS.CLOSED:
			returnValue = porCapturar; 
			break;
		case CONSTANTS.CONTAINER_STATUS.OPEN:
			returnValue = porCapturar; 
			break;
		case CONSTANTS.CONTAINER_STATUS.VALID:
			returnValue = completo;
			break;

	}

	helper.Log("CustomStatus "+_value.containerId+" fin: " + returnValue);

	return returnValue;
})(_v_);