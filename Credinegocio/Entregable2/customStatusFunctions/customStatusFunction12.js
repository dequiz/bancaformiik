(function(_value){
	
	helper.Log("CustomStatus "+_value.containerId+" inicio: " + getContainerCustomStatus(_value.containerId));  
	const porTomarDecision = 0;
	const creditoAprobado = 1;
	const creditoRechazado = 2;
	const devuelto = 3;

	const porTomarDecisionHijo = 0;
	const creditoAprobadoHijo = 1;
	const creditoRechazadoHijo = 2;
	const devueltoHijo = 3;

	const requisitoTomaDecision = "100-00000-0000-0121"

	var returnValue = 0;
	var statusHijo = getContainerCustomStatus(requisitoTomaDecision);
	
	switch(statusHijo) {
		case porTomarDecisionHijo:
			returnValue = porTomarDecision; 
			break;
		case creditoAprobadoHijo:
			returnValue = creditoAprobado; 
			break;
		case creditoRechazadoHijo:
			returnValue = creditoRechazado;
			break;
		case devueltoHijo:
			returnValue = devuelto;
			break;

	}

	helper.Log("CustomStatus "+_value.containerId+" fin: " + returnValue);

	return returnValue;
})(_v_);