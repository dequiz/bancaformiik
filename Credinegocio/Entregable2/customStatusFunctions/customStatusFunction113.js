(function(_value){
	
	helper.Log("CustomStatus "+_value.containerId+" inicio: " + getContainerCustomStatus(_value.containerId));  
	const porCapturar = 0;
	const porValidar = 1;
	const validado = 2;
	const rechazado = 3;

	var returnValue = 0;
	var status = getContainerStatus(_value.containerId);
	
	switch(status) {
		case CONSTANTS.CONTAINER_STATUS.CLOSED:
			returnValue = porCapturar; 
			break;
		case CONSTANTS.CONTAINER_STATUS.OPEN:
			returnValue = porCapturar; 
			break;
		case CONSTANTS.CONTAINER_STATUS.TOCONFIRM:
			returnValue = porValidar;
			break;
		case CONSTANTS.CONTAINER_STATUS.CONFIRMED:
			returnValue = validado;
			break;
		case CONSTANTS.CONTAINER_STATUS.REJECTED:
			returnValue = rechazado;
			break;

	}

	helper.Log("CustomStatus "+_value.containerId+" fin: " + returnValue);

	return returnValue;
})(_v_);