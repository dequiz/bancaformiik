(function(_value){
	
	helper.Log("CustomStatus "+_value.containerId+" inicio: " + getContainerCustomStatus(_value.containerId));  
	const porTomarDecision = 0;
	const creditoAprobado = 1;
	const creditoRechazado = 2;
	const devuelto = 3;
	const decisionControl = "ResultadoDecision";
	const acceptedValue = "0";
	const rejectedValue = "1"; 
	const returnedValue = "2";
	const requisitoInformacion = "100-00000-0000-0112";

	var returnValue = 0;
	var status = getContainerStatus(_value.containerId);
	
	switch(status) {
		case CONSTANTS.CONTAINER_STATUS.CLOSED:
			var valorResultado = getValueByFullName(_value.containerId, decisionControl); 
			
			if (valorResultado !== null) {
				helper.Log("121CS valorResultado: " + valorResultado[0]);
				switch (valorResultado[0]) {
					case acceptedValue: 
						returnValue = creditoAprobado; 
						break; 
					case rejectedValue:
						returnValue = creditoRechazado;
						break; 
					case returnedValue: 
						returnValue = devuelto;
						break;
				}
			}
			break; 
		case CONSTANTS.CONTAINER_STATUS.OPEN:
			returnValue = porTomarDecision; 
			break;
	}

	helper.Log("CustomStatus "+_value.containerId+" fin: " + returnValue);

	return returnValue;
})(_v_);