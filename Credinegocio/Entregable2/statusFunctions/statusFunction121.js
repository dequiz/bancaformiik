(function(_value){

	const requisitoInfoPersonal = "100-00000-0000-0112";
	const estadoReopened = "6";
	const aprobado = "1";
	const rechazado = "2"; 
	
	helper.Log("Cambio estado " + _value.containerId + " inicio: "+getContainerStatus(_value.containerId));
	var returnValue = getContainerStatus(_value.containerId);
	
	switch(returnValue) {
		case CONSTANTS.CONTAINER_STATUS.CLOSED:

			var esRechazado = getContainerCustomStatus(_value.containerId) == rechazado
			var esAprobado = getContainerCustomStatus(_value.containerId) == aprobado

			if (!esRechazado && !esAprobado) {

				if (getContainerStatus("100-00000-0000-0011") == CONSTANTS.CONTAINER_STATUS.VALID) {

					returnValue = CONSTANTS.CONTAINER_STATUS.OPEN; 
				} else {
					if (getContainerCustomStatus(requisitoInfoPersonal) == estadoReopened) {
						saveValueHelper(_value.containerId, {"ResultadoDecision":null});
					}
				}
			}
			break;
		case CONSTANTS.CONTAINER_STATUS.OPEN:
			helper.Log("121- IsNull:"+!isNull(_value.containerId,["MontoAprobado", "NumeroCuotas", "ValorCuotas", "FormaPago", "ResultadoDecision"]));
			helper.Log("121- IsNull:"+!isEmpty(_value.containerId,["MontoAprobado", "NumeroCuotas", "ValorCuotas", "FormaPago", "ResultadoDecision"]));
			if (!isNull(_value.containerId,["MontoAprobado", "NumeroCuotas", "ValorCuotas", "FormaPago", "ResultadoDecision"])
			 && !isEmpty(_value.containerId,["MontoAprobado", "NumeroCuotas", "ValorCuotas", "FormaPago", "ResultadoDecision"])) {
				returnValue = CONSTANTS.CONTAINER_STATUS.CLOSED; 
			}
			break;
		default:
			returnValue = getContainerStatus(_value.containerId);
	} 

	helper.Log("Cambio estado "+_value.containerId+" fin: "+returnValue);
	
	return returnValue;
})(_v_);