(function(_value){

	const requisitoInfoPersonal = "100-00000-0000-0112";
	
	helper.Log("Cambio estado " + _value.containerId + " inicio: " + getContainerStatus(_value.containerId));
	var returnValue = getContainerStatus(_value.containerId);
	
	switch(returnValue) {
		case CONSTANTS.CONTAINER_STATUS.CLOSED:
			if (getContainerStatus(requisitoInfoPersonal) == CONSTANTS.CONTAINER_STATUS.TOCONFIRM) {
				returnValue = CONSTANTS.CONTAINER_STATUS.OPEN; 
			}
			break;
		case CONSTANTS.CONTAINER_STATUS.OPEN:
			if (!isNull(_value.containerId,["TipoNegocio", "ActividadSector", "NombreNegocio", "TiempoExperiencia", "TiempoOperacion", "InscritoCamaraComercio", "EmpleadosFijos", "EmpleadosTemporales", "IngresosMensuales", "EgresosMensuales", "ReferenciaUbicacion"]) 
				&& !isEmpty(_value.containerId,["TipoNegocio", "ActividadSector", "NombreNegocio", "TiempoExperiencia", "TiempoOperacion", "InscritoCamaraComercio", "EmpleadosFijos", "EmpleadosTemporales", "IngresosMensuales", "EgresosMensuales", "ReferenciaUbicacion"])) {
				returnValue = CONSTANTS.CONTAINER_STATUS.TOCONFIRM; 
			}
			break;
		case CONSTANTS.CONTAINER_STATUS.TOCONFIRM:
			if (isValid(_value.containerId,["TipoNegocio", "ActividadSector", "NombreNegocio", "TiempoExperiencia", "TiempoOperacion", "InscritoCamaraComercio", "EmpleadosFijos", "EmpleadosTemporales", "IngresosMensuales", "EgresosMensuales", "ReferenciaUbicacion"])) {
				returnValue = CONSTANTS.CONTAINER_STATUS.CONFIRMED; 
			} 
			else {
				returnValue = CONSTANTS.CONTAINER_STATUS.REJECTED; 
			}
			break;
		case CONSTANTS.CONTAINER_STATUS.CONFIRMED:
			if (getContainerStatus(requisitoInfoPersonal) == CONSTANTS.CONTAINER_STATUS.TOCONFIRM) {
				returnValue = CONSTANTS.CONTAINER_STATUS.OPEN; 
			}
		default:
			returnValue = getContainerStatus(_value.containerId)
	} 

	helper.Log("Cambio estado " + _value.containerId + " fin: " + getContainerStatus(_value.containerId));
	
	return returnValue;
})(_v_);