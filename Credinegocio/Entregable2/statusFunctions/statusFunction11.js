(function(_value){

	helper.Log("Cambio estado "+_value.containerId+" inicio: "+getContainerStatus(_value.containerId));
	var returnValue = getContainerStatus(_value.containerId);
	
	switch(returnValue) {
		case CONSTANTS.CONTAINER_STATUS.CLOSED:
			if (getContainerStatus("100-00000-0000-0114") == CONSTANTS.CONTAINER_STATUS.VALID) {
				returnValue = CONSTANTS.CONTAINER_STATUS.VALID; 
			}
			break;
		case CONSTANTS.CONTAINER_STATUS.OPEN:
			if (getContainerStatus("100-00000-0000-0114") == CONSTANTS.CONTAINER_STATUS.VALID) {
				returnValue = CONSTANTS.CONTAINER_STATUS.VALID; 
			}
			break;
		default:
			returnValue = getContainerStatus(_value.containerId)
	}

	helper.Log("Cambio estado "+_value.containerId+" fin: "+getContainerStatus(_value.containerId));
	
	return returnValue;
})(_v_);